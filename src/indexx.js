var current = document.getElementById('current');  
var options = { 
    silent: true, 
    printBackground: true, 
    color: false, 
    margin: { 
        marginType: 'printableArea'
    }, 
    landscape: false, 
    pagesPerSheet: 1, 
    collate: false, 
    copies: 1, 
    header: 'Encabezado de la página', 
    footer: 'Pie de pagina'
} 
  
current.addEventListener('click', (event) => { 
    let win = BrowserWindow.getFocusedWindow(); 
    // let win = BrowserWindow.getAllWindows()[0]; 
  
    win.webContents.print(options, (success, failureReason) => { 
        if (!success) console.log(failureReason); 
  
        console.log('Print Initiated'); 
    }); 
}); 