const electron = require('electron') 
// Importing BrowserWindow from Main 
const BrowserWindow = electron.remote.BrowserWindow; 
  
var url = document.getElementById('url'); 
var options = { 
    silent: true, 
    printBackground: true, 
    color: false, 
    margin: { 
        marginType: 'printableArea'
    }, 
    landscape: false, 
    pagesPerSheet: 1, 
    collate: false, 
    copies: 1, 
    header: 'Encabezado de la página', 
    footer: 'Pie de pagina'
} 
  
url.addEventListener('click', (event) => { 
    // Defining a new BrowserWindow Instance 
    let win = new BrowserWindow({ 
        show: false, 
        webPreferences: { 
            nodeIntegration: true
        } 
    }); 
    win.loadURL('https://www.youtube.com/'); 
  
    win.webContents.on('did-finish-load', () => { 
        win.webContents.print(options, (success, failureReason) => { 
            if (!success) console.log(failureReason); 
            console.log('Print Initiated'); 
        }); 
    }); 
}); 